# ToDo

- [x] Diploma-app v0.1
- [x] Dockerfile
- [x] Separate front and back
- [x] Refactore block database
- [x] Make Stress test
- [x] Create Gitlab
	+ [x] CI
	+ [x] CD
- [x] Terraform IaC AWS code
	+ [x] RDS
	+ [x] EKS
- [x] Monitoring
- [x] Logging
- [ ] Tests
- [ ] Presentation

