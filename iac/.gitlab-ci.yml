image: registry.gitlab.com/gitlab-org/terraform-images/stable:latest
variables:
  TF_ROOT: ${CI_PROJECT_DIR}/iac
  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${CI_PROJECT_NAME}

cache:
  key: tf-pipeline
  paths:
    - ${TF_ROOT}/.terraform

before_script:
  - cd ${TF_ROOT}

stages:
  - prepare
  - validate
  - build
  - deploy
  - destroy

init:
  stage: prepare
  script:
    - gitlab-terraform init

validate:
  stage: validate
  script:
    - gitlab-terraform init
    - gitlab-terraform validate
  dependencies:
    - init
  needs: ["init"]

plan:
  stage: build
  script:
    - gitlab-terraform plan -var="db_name=${TF_VAR_db_name}" -var="db_user=${TF_VAR_db_user}" -var="db_password=${TF_VAR_db_password}"
    - gitlab-terraform plan-json --auto-approve -var="db_name=${TF_VAR_db_name}" -var="db_user=${TF_VAR_db_user}" -var="db_password=${TF_VAR_db_password}"
  dependencies:
    - validate
  needs: ["validate"]
  artifacts:
    name: plan
    paths:
      - ${TF_ROOT}/plan.cache
    reports:
      terraform: ${TF_ROOT}/plan.json

# Separate apply job for manual launching Terraform
apply:
  extends: plan
  stage: deploy
  environment:
    name: production
  script:
    - gitlab-terraform apply --auto-approve
  needs: ["plan"]
  dependencies:
    - plan
  when: manual
  only:
    - dev

destroy:
  extends: apply
  stage: destroy
  script:
    - gitlab-terraform destroy --auto-approve
  needs: ["plan","apply"]
  dependencies:
    - apply
  when: manual
  only:
    - dev