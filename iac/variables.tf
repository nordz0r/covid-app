variable "db_name" {
  description = "Database name"
  type = string
}

variable "db_user" {
  description = "Database user"
  type = string
}

variable "db_password" {
  description = "Database password"
  type = string
}


## Tags
variable "tags" {
  type = map
  default = {
    Owner   = "andrei_scheglov@epam.com"
    Project = "Diploma"
  }
  description = "Default tags"
}